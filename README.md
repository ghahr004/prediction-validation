### Introduction
I was given a coding challenge for my application for the Insight Data Engineering. 

The challenge is working on a financial institution analysis that analyzes real-time stock market data. To determine the best trading strategy, the 
company's data scientists created a machine learning model to predict the future price of a stock every hour, and they want to test it on 
real-time stock data. 

Before deploying the model, they want to help test how accurate their predictions are over time by comparing their predictions with newly 
arriving real-time stock prices. In order to do that, there will be three files:

1. `actual.txt`: A time-ordered file listing the actual value of stocks at a given hour.
1. `predicted.txt`: A time-ordered file of the predicted value of certain stocks at a given hour. 
1. `window.txt`: Holds a single integer value denoting the window size (in hours) for which you must calculate the `average error`.

The file `window.txt` is just a single integer which indicates the window length in which we want to know the average error while the 
`actual.txt` together with its counterpart `predicted.txt` are of the form `hour|stock ID|price`  the price is the actual
price for a stock market at some hour while in predicted it is the predicted price. 

My  output file is named `comparison.txt` where each line has the following pipe-delimited fields:

1. Starting hour time window
1. Ending hour time window
1. `average error` rounded off to 2 decimal places. pected value.

### Summery of Approach 

After reading files `window.txt`, `actual.txt` and `predicted.txt` we convert `window.txt` into an integer in the window variable we define.

Since lines in `actual.txt` and `predicted.txt` are of the form `hour|stock ID|price` we define two similar dictionaries for 
`actual.txt` and `predicted.txt` and name them as `dict_of_actual` and `dict_of_predicted`. Elements of `dict_of_actual` is of the form 
`(int(hour), stock ID): price` and the same happens for its companion `predicted.txt`. Next, we join these two dictionaries in a way that 
the keys are of the from `(int(hour), stock ID)` where  `(int(hour), stock ID)` exists in both `dict_of_actual.keys()` and 
`dict_of_predicted.keys()`
and the values are `abs(dict_of_actual[a] - dict_of_predicted[a])` which calculates the diffrenece of the prices in the `actual.txt` and
`predicted.txt`. We name this dictionary as `actualpredictedjoined`. 

Next, we will calculate the average of errors using the function `average_of_errors(i,w)` by accumumlating the errors in a the time
window of length `w`. The function `average_of_errors(i,w)` gives the average of errors starting from hour `i` to `i+w-1` if the 
list of all errors combined is not empty otherwise it gives `NA`.

Since taking float of numbers such as `2.10` gives `2.1` and in the output has to be of the form 
`i|i+window-1|absolute value of  difference in prices of actual and predicted` and the 
absolute value of  difference in prices of actual and predicted
has to be rounded to hundredth we define the  function `format_float` which gives string of the float
if it is of the form `2.12` or `3.12` but gives `2.10` to a float of the form
`2.1` therefore it meets the requirments of the output.





