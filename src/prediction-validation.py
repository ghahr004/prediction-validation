import sys

# First we read the files where actual indicates the actual value of each stock 
# predicated indicates the actual value of each stock and window is a single
# integer representing the specified sliding time.

window = int(open(str(sys.argv[1]), "r").read().splitlines()[0])
actual = open(str(sys.argv[2]), "r").read().splitlines()
predicted = open(str(sys.argv[3]), "r").read().splitlines()

# Since the lines in actual and predicted are of the form 
#'hour|stock ID|price' in which in the actual file, the price is the actual
# price while in predicted it is the predicted price we need to
# split both of the files in the same procedure. 

# For the sake of simplicity we define two dictionaries out of actual and predicted 
# whose keys are of the form (int(hour), stock ID) and values are 
# float(price) associated to the the stock ID and hour. Therefore
# we define the following dictionaries.


dict_of_actual = {(int(rec.split('|')[0]),rec.split('|')[1]):float(rec.split('|')[2]) for rec in actual}
dict_of_predicted = {(int(rec.split('|')[0]),rec.split('|')[1]):float(rec.split('|')[2]) for rec in predicted}


# Since we compare every stock and time pair in actual.txt with its companion in predicted.txt and calculate the error between them,
# we need to join dict_of_actual and dict_of_predicted based on the keys and then calculate the absolute value of the
# difference between the prices. Note that in each hour the prices that happen to be in both actual.txt and
# predicted.txt will be joined and then the difference between the prices will be calculate. 
# In the next three lines, we first put the keys of dict_of_actual amd dict_of_predicted into 
# separate sets and calculate the intersection of sets and then convert the intersection as a list.

set_of_actual_keys = set(dict_of_actual.keys())
set_of_predicted_keys = set(dict_of_predicted.keys())
list_of_keys_in_intersection = list(set_of_actual_keys.intersection(set_of_predicted_keys))
 
# In actualpredictedjoined we get the list of elements of the form ((hour, stock ID), absolute value of difference between prices) 
# provided that the (hour, stock ID) exist in both actual and predicted inputs.


actualpredictedjoined = [(a, abs(dict_of_actual[a] - dict_of_predicted[a])) for a in list_of_keys_in_intersection]

# Based on the instructions given in the challenge, we need to find the average error in a time window.
# In the following function, we assume that w is the time window and i stands for the hour.
# The command map(lambda rec: rec[1], filter(lambda rec: rec[0][0] == i, actualpredictedjoined)) gives us
# error for time i. When the list of  errors in a time window is empty, based on the
# instructions given, we need to produce 'NA' otherwise it will calculate the average of the error 
# in the time window of i to i+w-1 rounded off by 2.


def average_of_errors(i,w):
    s = []
    for j in range(w):
        s = s + map(lambda rec: rec[1], filter(lambda rec: rec[0][0] == i+j, actualpredictedjoined))
    if len(s) == 0:
        return 'NA'
    else:
        return round(float(sum(s))/len(s),2)

# We need to find the maximum hour given in the actual.txt and the following command will do it.        
    
max_hour = max(map(lambda rec: rec[0][0], dict_of_actual.items()))

# Since taking float of numbers such as 2.10 gives 2.1 and in the output has to be of the form 
# 'i|i+window-1|absolute value of  difference in prices of actual and predicted' and the 
# absolute value of  difference in prices of actual and predicted
# has to be rounded to hundredth we define the following function which gives string of the float
# if it is of the form 2.12 or 3.12 but gives 2.10 to a float of the form
# 2.1 therefore it meets the requirments of the output. Moreover, format_float gives string for every string 
# value just in case that it is given 'NA' as it will operate on average_of_errors and average_of_errors 
# can give 'NA' in case that the the errors in a time window is empty.

def format_float(f):
    if isinstance(f, float) == True:
        g = str(f)
        index_of_dot = g.index('.')
        f_and_dot = g[:index_of_dot+1]
        decimals = g[index_of_dot+1:]
        if len(decimals) == 1:
            return g + '0'
        else:
            return f
    else:
        return f

output = open(str(sys.argv[4]),'w')

for i in range(1,max_hour - window + 2):
    output.write(str(i) + '|' + str(i+ window - 1) + '|' + str(format_float(average_of_errors(i,window))) + "\n")
output.close()